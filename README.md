# WP BBT Theme

WP BBT Theme is a custom wordpress website specifically made for BBT Team.

## Plugins

Wordpress plugins used for this is site are listed below

| Plugin |   Version |  Link |
| ------ |  ------ | ------ |
| Gravity Forms | 2.3.2 |  [Visit plugin site](https://www.gravityforms.com/) |
| WPBakery Page Builder | 6.0.5 | [Visit plugin site](http://wpbakery.com/) |
| Advanced Custom Fields PRO | 5.6.10 | [Visit plugin site](https://www.advancedcustomfields.com/pro/) |

Please refer to the BBT knowledge base page on [How to create a wordpress site in you local machine]
(https://bbtdigital.atlassian.net/wiki/spaces/BBTKB/pages/102465611/How+to+create+a+wordpress+site+in+your+local+machine)

Please refer to [Bedrock](https://roots.io/bedrock/), if you have issues on your installation

Check the plugins and update them to the latest version  