<?php

namespace BBT;


class ThemeSettings
{
    function __construct() {
        $this->includes();
        $this->register_menu();

        add_action('acf/init', [ $this, 'theme_setting_fields' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ]  );


    }

    public function includes()
    {
        //All additional webhook filter should be added in the theme-setting-helper file
        include STYLESHEETDIR.'/includes/theme-settings-helper.php';

        //All custom function to be used in the site should be added here
        include STYLESHEETDIR.'/includes/wp-helper-functions.php';
    }

    public function enqueue_scripts() {
        global $post;

        if (is_admin()) {
            return;
        }

        wp_enqueue_style('style', get_stylesheet_uri());
        wp_enqueue_style('vendor', THEMEDIR . '/assets/dist/css/vendor.min.css');
        wp_enqueue_style('theme-global', THEMEDIR . '/assets/dist/css/globals.min.css', false, date('dmYh'));

        /*
         * Only use styles for homepage to avoid loading the whole css in homepage
         */
        if (is_front_page()) {
            wp_enqueue_style('main', THEMEDIR . '/assets/dist/css/home.min.css', false, date('dmYh'));
        } else {
            wp_enqueue_style('main', THEMEDIR . '/assets/dist/css/style.min.css', false, date('dmYh'));
        }

        wp_enqueue_script('vendor',  THEMEDIR.'/assets/dist/js/vendor.min.js', array(), false,true );
        wp_enqueue_script('main', THEMEDIR . '/assets/dist/js/scripts.min.js', array(), date('dmYh'), true);
    }

    public function theme_setting_fields(){

        if (!function_exists('acf_add_local_field_group')){
            return false;
        }

        acf_add_options_page([
            'page_title' => 'Theme General Settings',
            'menu_title' => 'Theme Settings',
            'menu_slug' => 'theme-general-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ]);

        acf_add_options_sub_page([
            'page_title' => 'Theme Scripts',
            'menu_title' => 'Scripts',
            'parent_slug' => 'theme-general-settings',
        ]);


        acf_add_local_field_group([
            'key' => 'group_theme_settings_scripts',
            'title' => 'SCRIPTS -- Should be wrapped in script or noscript tags or comments only',
            'fields' => [
                [
                    'key' => 'field_google_optimize',
                    'label' => 'Google Optimize',
                    'name' => 'google_optimize',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>'
                ],
                [
                    'key' => 'field_google_tag_manager',
                    'label' => 'Google Tag Manager',
                    'name' => 'google_tag_manager',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>',
                    'instructions' => 'Code will display in the < head> tag'
                ],
                [
                    'key' => 'field_google_tag_manager_noscript',
                    'label' => 'Google Tag Manager (noscript)',
                    'name' => 'google_tag_manager_noscript',
                    'type' => 'textarea',
                    'placeholder' => '<noscript></noscript>',
                    'instructions' => 'Code immediately appear after the opening of < body> tag'
                ],
                [
                    'key' => 'field_facebook_pixel_header',
                    'label' => 'Facebook Pixel (Header)',
                    'name' => 'facebook_pixel_header',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>'
                ],
                [
                    'key' => 'field_facebook_pixel_body',
                    'label' => 'Facebook Pixel (Body)',
                    'name' => 'facebook_pixel_body',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>'
                ],
                [
                    'key' => 'field_google_analytics',
                    'label' => 'Google Analytics',
                    'name' => 'google_analytics',
                    'type' => 'textarea',
                ],
                [
                    'key' => 'field_custom_before_header',
                    'label' => 'Custom Script - Before < /head> Tag',
                    'name' => 'custom_before_header',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>'
                ],
                [
                    'key' => 'field_custom_after_body',
                    'label' => 'Custom Script - After Body Tag < body > Tag ',
                    'name' => 'custom_after_body',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>'
                ],
                [
                    'key' => 'field_footer_script',
                    'label' => 'Custom Script - Footer Script ',
                    'name' => 'footer_script',
                    'type' => 'textarea',
                    'placeholder' => '<script></script>'
                ]
            ],
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'acf-options-scripts',
                    ]
                ]
            ]
        ]);
        acf_add_local_field_group([
            'key' => 'group_theme_settings_header',
            'title' => 'Header Group',
            'fields' => [
                [
                    'key' => 'field_header_logo',
                    'label' => 'Header Logo',
                    'name' => 'header_logo',
                    'type' => 'image',
                ]
            ],
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'theme-general-settings',
                    ]
                ]
            ]
        ]);
        acf_add_local_field_group([
            'key' => 'group_theme_settings_footer',
            'title' => 'Footer Group',
            'fields' => [
                [
                    'key' => 'field_footer_logo',
                    'label' => 'Footer Logo',
                    'name' => 'footer_logo',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_copyright',
                    'label' => 'Copyright',
                    'name' => 'copyright',
                    'type' => 'text',
                ]
            ],
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'theme-general-settings',
                    ]
                ]
            ]
        ]);
        acf_add_local_field_group([
            'key' => 'group_theme_settings_SEO',
            'title' => 'SEO Requirements',
            'fields' => [
                [
                    'key' => 'field_favicon',
                    'label' => 'Favicon',
                    'name' => 'seo_favicon',
                    'type' => 'image',
                    'return_format' => 'url',
                    'instructions' => 'It must point to a non-transparent 192px (or 180px) square PNG'
                ],
                [
                    'key' => 'field_seo_thumb',
                    'label' => 'Thumbnails meta',
                    'name' => 'seo_thumb',
                    'type' => 'image',
                    'return_format' => 'url'
                ],
                [
                    'key' => 'field_color_thumb',
                    'label' => 'Theme Color meta',
                    'name' => 'color_thumb',
                    'type' => 'text',
                    'instructions' => 'Should be in Hex format #FFFFFF'
                ]
            ],
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'theme-general-settings',
                    ]
                ]
            ]
        ]);
        acf_add_local_field_group([
            'key' => 'cfpf',
            'title' => 'Custom Body Class',
            'fields' => [
                [
                    'key' => 'custom_body_class',
                    'label' => 'CSS Class',
                    'name' => 'custom_body_class',
                    'type' => 'text',
                ]
            ],
            'position' => 'side',
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ]
                ]
            ],
        ]);
    }

    public function register_menu(){
        register_nav_menus(['header-menu' => __('Header Menu')]);
    }

}