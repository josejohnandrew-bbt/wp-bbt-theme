<?php

define('THEMEDIR', get_template_directory_uri());
define('STYLESHEETDIR', get_stylesheet_directory());

use BBT\ThemeSettings;
include 'classes/Theme.php';


if ( ! function_exists( 'load_theme_setting' ) ) {
    function load_theme_setting()
    {
        return new ThemeSettings();
    }
    load_theme_setting();
}

/*
require_once  'includes/theme-settings.php';


require_once  'includes/wp-hooks.php';



require_once  'includes/wp-helper-functions.php';

*/


