<header class="sticky-top">
	<?php /*Desktop header*/ ?>
	<div class="header d-none d-md-block">
		<div class="container dt">
			<div class="row">
				<div class="col-md-12">
					<a class="" href="<?php echo get_home_url(); ?>">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo.png" alt="" width="200" height="auto" class="img-fluid logo">
					</a>
				</div>
				<div class="col-md-8 ml-auto text-right">
				
				</div>
			</div>
		</div>
	</div>
	<?php /*Main Navigation*/ ?>
	<div class="main-navigation d-none d-md-block">
		<div class="container">
			<?php 
		    	wp_nav_menu( array(
		    		'container' => false,
				    'theme_location' => 'header-menu' ,
				    'menu_class'   => 'main-nav',
				) );
		    ?>
		</div>
	</div>

	
	<?php /*Mobile header*/ ?>
	<div class="container-fluid d-block d-md-none">
		<div class="row">
			<div class="col-6">
				<a class="" href="<?php echo get_home_url(); ?>">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo.png" alt="" width="200" height="auto"  class="img-fluid logo">
				</a>
			</div>
			<div class="col-6 d-flex align-items-center justify-content-end">
                <div id="menuToggle">

                    <input type="checkbox" />
                    <span></span>
                    <span></span>
                    <span></span>

                </div>
			</div>

		<div class="navigation" id="navbar-main" style="display: none" >
		  	<?php
		    	wp_nav_menu( array(
		    		'container' => false,
				    'theme_location' => 'header-menu' ,
				    'menu_class'   => 'navbar-nav mr-auto',
				) );
		    ?>

		</div>
		</div>
	</div>
	
	
	
</header>

