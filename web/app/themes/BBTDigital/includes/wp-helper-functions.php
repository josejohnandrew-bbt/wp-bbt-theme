<?php

/*
 *
 * Redirect Site is using IE 11 and below
 * Make sure to create a page /browsers/ offering a downloads of latest browsers
 *
 */
if( ( isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false ))){

    $URL = get_home_url()."/browsers/";
    $URI = $_SERVER['REQUEST_URI'];

    //Avoid infinite loop
    if (get_home_url().$URI === $URL){
        return;
    }

    echo("<script>location.href='$URL'</script>");
}

function ts_seo_body(){
    if (WP_ENV == 'development') {
        return false;
    }

    if (get_field('google_tag_manager_noscript', 'option')) {
        echo get_field('google_tag_manager_noscript', 'option');
    }
    if (get_field('facebook_pixel_body', 'option')) {
        echo get_field('facebook_pixel_body', 'option');
    }
    return true;

}

function ts_seo_header(){

    if (get_option('seo_favicon', 'option')) {
        echo '<link rel = "shortcut icon" href = "'.get_field('seo_favicon', 'option').'" type = "image/x-icon" >';
        echo '<link rel = "apple-touch-icon" href = "'.get_field('seo_favicon', 'option').'" >';
        echo '<meta name = "msapplication-square310x310logo" content = "'.get_field('seo_favicon', 'option').' >';
    }

    if (WP_ENV == 'development') {
        echo '<meta name="robots" content="noindex">';
        return;
    }

    if (get_option('seo_thumb', 'option')) {
        echo '<meta name = "theme-color" content = "'.get_field('seo_thumb', 'option').'" />';
    }
    if (get_option('color_thumb', 'option')) {
        echo '<meta name="thumbnail" content="'.get_field('color_thumb', 'option').'"/>';
    }

    return true;
}

function ts_scripts_header(){
    if (get_field('google_optimize', 'option')) {
        echo get_field('google_optimize', 'option');
    }
    if (get_field('google_analytics', 'option')) {
        echo get_field('google_analytics', 'option');
    }
    if (get_option('google_tag_manager', 'option')) {
        echo get_field('google_tag_manager', 'option');
    }
    if (get_option('facebook_pixel_header', 'option')) {
        echo get_field('facebook_pixel_header', 'option');
    }

    if (get_option('custom_before_header', 'option')) {
        echo get_field('custom_before_header');
    }
}

function ts_scripts_body(){

    if (get_option('custom_after_body', 'option')) {
        echo get_field('custom_after_body', 'option');
    }

}

function ts_custom_body_class( ){
    $body_class = '';
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    if (stripos( $user_agent, 'Safari') !== false && stripos( $user_agent, 'Chrome') === false) {
        $body_class = "browser-safari ";
    }


    if (get_option('custom_body_class', 'option')) {
        $body_class .= get_field('custom_body_class');
        $body_class .= ' ';
    }

    return $body_class;
}