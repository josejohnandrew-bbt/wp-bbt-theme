<?php
/*
 * Generate Custom body class
 */
add_filter('body_class', 'add_slug_body_class');
function add_slug_body_class($classes){
    global $post;
    $isMobile = '';
    if (wp_is_mobile()) {
        $isMobile = 'is-mobile';
    }
    //add page title class
    if (isset($post)) {
        $classes[] = $isMobile . ' ' . $post->post_type . '-' . $post->post_name;
        if ($post->post_parent) {
            $post_data = get_post($post->post_parent);
            $parent_slug = $post_data->post_name;
            $classes[] = $post->post_type . '-' . $parent_slug;
        }
    }
    return $classes;
}

//Disable update wordpress nag
add_action('admin_head', 'hide_update_notice_to_all_but_admin_users', 1);
function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('update_core')) {
        remove_action('admin_notices', 'update_nag', 3);
    }
}

//Remove wordpress version number
add_filter('the_generator', 'wpb_remove_version');
function wpb_remove_version()
{
    return '';
}

//Hide login errors in wordpress
add_filter('login_errors', 'no_wordpress_errors');
function no_wordpress_errors()
{
    return 'Something is wrong!';
}

// Remove welcome to wordpress from dashboard
remove_action('welcome_panel', 'wp_welcome_panel');

// Disable XML-RPC in Wordpress
add_filter('xmlrpc_enabled', '__return_false');

// Add custom logo to wp login
add_action('login_enqueue_scripts', 'wpb_login_logo');
function wpb_login_logo()
{
    global $post; ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ?>/assets/images/admin-logo.png);
            background-repeat: no-repeat;
            background-size: 300px 100px;
            height: 100px;
            padding-bottom: 10px;
            width: 300px;
        }

        #wp-submit {
            background: black;
            border: none;
            box-shadow: 0px 0px 0px transparent;
            text-shadow: none;
        }

        #login .message, #login .success, #login #login_error {
            border-left: 4px solid black;
        }
    </style>
<?php }





