# Overview

Blank wordpress theme with gulp 
Gulp includes scss minifier, js minifier, image optimization + live reload
### Installation

Make sure  [Node.js](https://nodejs.org/) is installed.

# Usage

Once the files are downloaded, run the npm install

```sh
$ npm install 
```

Run bower install to download theme dependecies

```sh
$ bower install 
```

After npm and bower are downloaded, configure your gulpfile.js assest location.

After gulpfile.js is configured, you can now run gulp as below

```sh
$ gulp
```


